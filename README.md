# **Memory Game**

<hr>

**But du jeu** : le but du jeu de memory est de collecter les paires correspondantes.

Retournez deux cartes afin de trouver deux fois le même personnages. Si les deux personnages ne sont pas identiques, les cartes se retourneront seront donc de nouveau cachées. Si elles sont identiques, les cartes resteront de face, ainsi, vous pourrez tenter de trouver d'autres personnages. 



<hr>

## User Stories

* En tant qu'utilisateur je veux pouvoir cliquer sur les cartes afin de les retourner
* En tant qu'utilisateur je veux pouvoir voir le timer pendant ma partie
* En tant qu'utilisateur et en cas de défaite, je veux pouvoir refresh la page afin de relancer le jeu en cliquant sur le game over

## Maquette

<hr>



![Maquette 2](./maquette/Maquette 2.png)

![Maquette jeu 3](./maquette/Maquette jeu 3.png)





## UML 

<hr>

#### UseCase

![UseCaseDiagram1](uml/UseCaseDiagram1.jpg)



###  Classes

![Classe](uml/Classe.jpg)



<hr>

## Demo

#####  Classes

![audiocontroller](img/audiocontroller.png)

<hr>

##### AudioController Methods

![classeSimplon](img/methodeAudio.png)

<hr>

##### Class

![classeSimplon](img/classeSimplon.png)

<hr>

##### SimplonBallZ Methods

![methodeSimplob](img/methodeSimplob.png)

<hr>

### Css

![overlay](img/overlay.png)

<hr>

##### Function

![checkForMatch](img/checkForMatch.png)

<hr>

##### HTML

![dataframework](img/dataframework.png)

<hr>

##### Function

![disableCards](img/disableCards.png)

<hr>

### Compétences visées :

- Maquetter une application
- POO : Programmation orienté objet
- Utilisation du boards et des milestones sur GitLab pour organiser les taches
- Javascript

